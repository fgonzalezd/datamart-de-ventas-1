package pages;

public class EnvironmentTabGenerales_Page extends Base_Page {

    public static String environment = "src/test/resources/query/Environment_TabGenerales.xml";

    /*************************
     ***** CREATE ORIGEN *****
     *************************/

    public static String createDIAS_FESTIVOS() {
        String create = fetchQuery(environment).getProperty("createDIAS_FESTIVOS");
        return create;
    }

    public static String createTIPO_CAMBIO() {
        String create = fetchQuery(environment).getProperty("createTIPO_CAMBIO");
        return create;
    }

    /****************************
     ***** DROPTABLE ORIGEN *****
     ****************************/

    public static String dropDIAS_FESTIVOS() {
        String create = fetchQuery(environment).getProperty("dropDIAS_FESTIVOS");
        return create;
    }

    public static String dropTIPO_CAMBIO() {
        String create = fetchQuery(environment).getProperty("dropTIPO_CAMBIO");
        return create;
    }

    /***********************
     ***** DELETE HIVE *****
     ***********************/

    public static String deleteDIAS_FESTIVOS() {
        String create = fetchQuery(environment).getProperty("deleteDIAS_FESTIVOS");
        return create;
    }

    public static String deleteTIPO_CAMBIO() {
        String create = fetchQuery(environment).getProperty("deleteTIPO_CAMBIO");
        return create;
    }

    public static String deleteFECHA_CALENDARIO() {
        String create = fetchQuery(environment).getProperty("deleteFECHA_CALENDARIO");
        return create;
    }

    /**********************
     ***** DELETE MCP *****
     **********************/

    public static String deletePROCESO_METRICA_REL() {
        String create = fetchQuery(environment).getProperty("deletePROCESO_METRICA_REL");
        return create;
    }

    public static String deleteEJECUCION_PROCESO() {
        String create = fetchQuery(environment).getProperty("deleteEJECUCION_PROCESO");
        return create;
    }
}
