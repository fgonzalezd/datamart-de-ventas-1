package pages;

public class InsertVentasComercio_Page extends Base_Page  {

    public static String environment = "src/test/resources/insert/Insert_VentasComercio.xml";

    /*************************
     ***** INSERT ORIGEN *****
     *************************/

    public static String insert_TX_PROCESADAS01() {
        String insert = fetchQuery(environment).getProperty("insert_TX_PROCESADAS01");
        return insert;
    }

    public static String insert_TX_PROCESADAS02() {
        String insert = fetchQuery(environment).getProperty("insert_TX_PROCESADAS02");
        return insert;
    }
}
