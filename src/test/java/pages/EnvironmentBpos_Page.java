package pages;

public class EnvironmentBpos_Page extends Base_Page{

    public static String environment = "src/test/resources/query/Environment_Bpos.xml";

    /*************************
     ***** CREATE ORIGEN *****
     *************************/

    public static String createEQUIPOXEST() {
        String create = fetchQuery(environment).getProperty("createEQUIPOXEST");
        return create;
    }

    /****************************
     ***** DROPTABLE ORIGEN *****
     ****************************/

    public static String dropEQUIPOXEST() {
        String create = fetchQuery(environment).getProperty("dropEQUIPOXEST");
        return create;
    }

    /**********************
     ***** DELETE RAW *****
     **********************/

    public static String deleteEQUIPOXEST() {
        String create = fetchQuery(environment).getProperty("deleteEQUIPOXEST");
        return create;
    }

    /**********************
     ***** DELETE MCP *****
     **********************/

    public static String deletePROCESO_METRICA_REL_EQUIPOXEST() {
        String create = fetchQuery(environment).getProperty("deletePROCESO_METRICA_REL_EQUIPOXEST");
        return create;
    }

    public static String deleteEJECUCION_PROCESO() {
        String create = fetchQuery(environment).getProperty("deleteEJECUCION_PROCESO");
        return create;
    }

    /**********************
     ***** DELETE LOG *****
     **********************/

    public static String logEQUIPOXEST() {
        String create = fetchQuery(environment).getProperty("logEQUIPOXEST");
        return create;
    }

    /***********************
     ***** DELETE HDFS *****
     ***********************/

    public static String hdfsEQUIPOXEST() {
        String create = fetchQuery(environment).getProperty("hdfsEQUIPOXEST");
        return create;
    }
}
