package pages;

public class InsertTxProcesadas_Page extends Base_Page  {

    public static String environment = "src/test/resources/insert/Insert_TxProcesadas.xml";

    /*************************
     ***** INSERT ORIGEN *****
     *************************/

    public static String insert_COMISION_RETENCION08() {
        String insert = fetchQuery(environment).getProperty("insert_COMISION_RETENCION08");
        return insert;
    }

    public static String insert_COMISION_RETENCION09() {
        String insert = fetchQuery(environment).getProperty("insert_COMISION_RETENCION09");
        return insert;
    }

    public static String insert_COMISION_RETENCION10() {
        String insert = fetchQuery(environment).getProperty("insert_COMISION_RETENCION10");
        return insert;
    }

    public static String insert_MONETARIAS08() {
        String insert = fetchQuery(environment).getProperty("insert_MONETARIAS08");
        return insert;
    }

    public static String insert_MONETARIAS09() {
        String insert = fetchQuery(environment).getProperty("insert_MONETARIAS09");
        return insert;
    }

    public static String insert_MONETARIAS10() {
        String insert = fetchQuery(environment).getProperty("insert_MONETARIAS10");
        return insert;
    }

    public static String insert_CONTRATOS08() {
        String insert = fetchQuery(environment).getProperty("insert_CONTRATOS08");
        return insert;
    }

    public static String insert_CONTRATOS09() {
        String insert = fetchQuery(environment).getProperty("insert_CONTRATOS09");
        return insert;
    }

    public static String insert_CONTRATOS10() {
        String insert = fetchQuery(environment).getProperty("insert_CONTRATOS10");
        return insert;
    }

    public static String insert_FECHA_CALENDARIO() {
        String insert = fetchQuery(environment).getProperty("insert_FECHA_CALENDARIO");
        return insert;
    }

    /**********************
     ***** INSERT MCP *****
     **********************/

    public static String insertPROCESO_METRICA_REL_N1() {
        String insert = fetchQuery(environment).getProperty("insertPROCESO_METRICA_REL_N1");
        return insert;
    }

    public static String insertEJECUCION_PROCESO_N1() {
        String insert = fetchQuery(environment).getProperty("insertEJECUCION_PROCESO_N1");
        return insert;
    }

    public static String insertPROCESO_METRICA_REL_N2() {
        String insert = fetchQuery(environment).getProperty("insertPROCESO_METRICA_REL_N2");
        return insert;
    }

    public static String insertEJECUCION_PROCESO_N2() {
        String insert = fetchQuery(environment).getProperty("insertEJECUCION_PROCESO_N2");
        return insert;
    }

    public static String insertPROCESO_METRICA_REL_N3() {
        String insert = fetchQuery(environment).getProperty("insertPROCESO_METRICA_REL_N3");
        return insert;
    }

    public static String insertEJECUCION_PROCESO_N3() {
        String insert = fetchQuery(environment).getProperty("insertEJECUCION_PROCESO_N3");
        return insert;
    }

    public static String insertPROCESO_METRICA_REL_N4() {
        String insert = fetchQuery(environment).getProperty("insertPROCESO_METRICA_REL_N4");
        return insert;
    }

    public static String insertEJECUCION_PROCESO_N4() {
        String insert = fetchQuery(environment).getProperty("insertEJECUCION_PROCESO_N4");
        return insert;
    }

    public static String insertPROCESO_METRICA_REL_N5() {
        String insert = fetchQuery(environment).getProperty("insertPROCESO_METRICA_REL_N5");
        return insert;
    }

    public static String insertEJECUCION_PROCESO_N5() {
        String insert = fetchQuery(environment).getProperty("insertEJECUCION_PROCESO_N5");
        return insert;
    }

    public static String insertPROCESO_METRICA_REL_N6() {
        String insert = fetchQuery(environment).getProperty("insertPROCESO_METRICA_REL_N6");
        return insert;
    }

    public static String insertEJECUCION_PROCESO_N6() {
        String insert = fetchQuery(environment).getProperty("insertEJECUCION_PROCESO_N6");
        return insert;
    }
}
