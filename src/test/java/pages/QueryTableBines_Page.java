package pages;

public class QueryTableBines_Page extends Base_Page{

    public static String queryXML = "src/test/resources/query/QueryTable_Bines.xml";

    /*********************
     ***** QUERY RAW *****
     *********************/

    public static String queryBIN_LOCAL() {
        String query = fetchQuery(queryXML).getProperty("queryBIN_LOCAL");
        return query;
    }

    public static String queryCOUNT_BIN_LOCAL() {
        String query = fetchQuery(queryXML).getProperty("queryCOUNT_BIN_LOCAL");
        return query;
    }

    public static String queryCLIENTE_EMISOR() {
        String query = fetchQuery(queryXML).getProperty("queryCLIENTE_EMISOR");
        return query;
    }

    public static String queryCOUNT_CLIENTE_EMISOR() {
        String query = fetchQuery(queryXML).getProperty("queryCOUNT_CLIENTE_EMISOR");
        return query;
    }

    /*********************
     ***** QUERY MCP *****
     *********************/

    public static String queryEJECUCION_PROCESO_BIN_LOCAL() {
        String query = fetchQuery(queryXML).getProperty("queryEJECUCION_PROCESO_BIN_LOCAL");
        return query;
    }

    public static String queryEJECUCION_PROCESO_COUNT_BIN_LOCAL() {
        String query = fetchQuery(queryXML).getProperty("queryEJECUCION_PROCESO_COUNT_BIN_LOCAL");
        return query;
    }

    public static String queryEJECUCION_PROCESO_CLIENTE_EMISOR() {
        String query = fetchQuery(queryXML).getProperty("queryEJECUCION_PROCESO_CLIENTE_EMISOR");
        return query;
    }

    public static String queryEJECUCION_PROCESO_COUNT_CLIENTE_EMISOR() {
        String query = fetchQuery(queryXML).getProperty("queryEJECUCION_PROCESO_COUNT_CLIENTE_EMISOR");
        return query;
    }
}
