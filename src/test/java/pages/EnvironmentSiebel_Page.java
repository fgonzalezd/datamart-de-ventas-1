package pages;

public class EnvironmentSiebel_Page extends Base_Page{

    public static String environment = "src/test/resources/query/Environment_Siebel.xml";

    /*************************
     ***** CREATE ORIGEN *****
     *************************/

    public static String createSPARTYPER() {
        String create = fetchQuery(environment).getProperty("createSPARTYPER");
        return create;
    }

    /****************************
     ***** DROPTABLE ORIGEN *****
     ****************************/

    public static String dropSPARTYPER() {
        String create = fetchQuery(environment).getProperty("dropSPARTYPER");
        return create;
    }

    /**********************
     ***** DELETE RAW *****
     **********************/

    public static String deleteSPARTYPER() {
        String create = fetchQuery(environment).getProperty("deleteSPARTYPER");
        return create;
    }

    /**********************
     ***** DELETE MCP *****
     **********************/

    public static String deletePROCESO_METRICA_REL_SPARTYPER() {
        String create = fetchQuery(environment).getProperty("deletePROCESO_METRICA_REL_SPARTYPER");
        return create;
    }

    public static String deleteEJECUCION_PROCESO() {
        String create = fetchQuery(environment).getProperty("deleteEJECUCION_PROCESO");
        return create;
    }

    /**********************
     ***** DELETE LOG *****
     **********************/

    public static String logSPARTYPER() {
        String create = fetchQuery(environment).getProperty("logSPARTYPER");
        return create;
    }

    /***********************
     ***** DELETE HDFS *****
     ***********************/

    public static String hdfsSPARTYPER() {
        String create = fetchQuery(environment).getProperty("hdfsSPARTYPER");
        return create;
    }

    /***********************
     ***** DELETE LOCK *****
     ***********************/

    public static String lockSPARTYPER() {
        String create = fetchQuery(environment).getProperty("lockSPARTYPER");
        return create;
    }

    /****************************
     ***** DELETE SECUENCIA *****
     ****************************/

    public static String secuenciaSPARTYPER() {
        String create = fetchQuery(environment).getProperty("secuenciaSPARTYPER");
        return create;
    }
}
