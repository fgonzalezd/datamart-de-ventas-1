package pages;

public class QueryTableArc_Page extends Base_Page {

    public static String queryXML = "src/test/resources/query/QueryTable_Arc.xml";

    /*********************
     ***** QUERY RAW *****
     *********************/

    public static String queryTIPO_RUBRO() {
        String query = fetchQuery(queryXML).getProperty("queryTIPO_RUBRO");
        return query;
    }

    public static String queryTIPO_RUBRO_COUNT() {
        String query = fetchQuery(queryXML).getProperty("queryTIPO_RUBRO_COUNT");
        return query;
    }

    public static String queryACTIVIDAD_AMEX() {
        String query = fetchQuery(queryXML).getProperty("queryACTIVIDAD_AMEX");
        return query;
    }

    public static String queryACTIVIDAD_AMEX_COUNT() {
        String query = fetchQuery(queryXML).getProperty("queryACTIVIDAD_AMEX_COUNT");
        return query;
    }

    public static String queryACTIVIDAD_ECONOMICA() {
        String query = fetchQuery(queryXML).getProperty("queryACTIVIDAD_ECONOMICA");
        return query;
    }

    public static String queryACTIVIDAD_ECONOMICA_COUNT() {
        String query = fetchQuery(queryXML).getProperty("queryACTIVIDAD_ECONOMICA_COUNT");
        return query;
    }

    public static String queryCONTRATOS() {
        String query = fetchQuery(queryXML).getProperty("queryCONTRATOS");
        return query;
    }

    public static String queryCONTRATOS_COUNT() {
        String query = fetchQuery(queryXML).getProperty("queryCONTRATOS_COUNT");
        return query;
    }

    public static String queryESTABLECIMIENTOS() {
        String query = fetchQuery(queryXML).getProperty("queryESTABLECIMIENTOS");
        return query;
    }

    public static String queryCOUNT_ESTABLECIMIENTOS() {
        String query = fetchQuery(queryXML).getProperty("queryCOUNT_ESTABLECIMIENTOS");
        return query;
    }

    public static String queryDIRECCIONES() {
        String query = fetchQuery(queryXML).getProperty("queryDIRECCIONES");
        return query;
    }

    public static String queryCOUNT_DIRECCIONES() {
        String query = fetchQuery(queryXML).getProperty("queryCOUNT_DIRECCIONES");
        return query;
    }

    public static String queryTIPO_CONTRATO() {
        String query = fetchQuery(queryXML).getProperty("queryTIPO_CONTRATO");
        return query;
    }

    public static String queryCOUNT_TIPO_CONTRATO() {
        String query = fetchQuery(queryXML).getProperty("queryCOUNT_TIPO_CONTRATO");
        return query;
    }

    public static String queryEECC_SPA() {
        String query = fetchQuery(queryXML).getProperty("queryEECC_SPA");
        return query;
    }

    public static String queryCOUNT_EECC_SPA() {
        String query = fetchQuery(queryXML).getProperty("queryCOUNT_EECC_SPA");
        return query;
    }

    public static String queryTIPO_CONTACTO() {
        String query = fetchQuery(queryXML).getProperty("queryTIPO_CONTACTO");
        return query;
    }

    public static String queryCOUNT_TIPO_CONTACTO() {
        String query = fetchQuery(queryXML).getProperty("queryCOUNT_TIPO_CONTACTO");
        return query;
    }

    /*********************
     ***** QUERY MCP *****
     *********************/

    public static String queryEJECUCION_PROCESO_TIPO_RUBRO() {
        String query = fetchQuery(queryXML).getProperty("queryEJECUCION_PROCESO_TIPO_RUBRO");
        return query;
    }

    public static String queryEJECUCION_PROCESO_COUNT_TIPO_RUBRO() {
        String query = fetchQuery(queryXML).getProperty("queryEJECUCION_PROCESO_COUNT_TIPO_RUBRO");
        return query;
    }

    public static String queryEJECUCION_PROCESO_ACTIVIDAD_AMEX() {
        String query = fetchQuery(queryXML).getProperty("queryEJECUCION_PROCESO_ACTIVIDAD_AMEX");
        return query;
    }
    public static String queryEJECUCION_PROCESO_COUNT_ACTIVIDAD_AMEX() {
        String query = fetchQuery(queryXML).getProperty("queryEJECUCION_PROCESO_COUNT_ACTIVIDAD_AMEX");
        return query;
    }

    public static String queryEJECUCION_PROCESO_ACTIVIDAD_ECONOMICA() {
        String query = fetchQuery(queryXML).getProperty("queryEJECUCION_PROCESO_ACTIVIDAD_ECONOMICA");
        return query;
    }

    public static String queryEJECUCION_PROCESO_COUNT_ACTIVIDAD_ECONOMICA() {
        String query = fetchQuery(queryXML).getProperty("queryEJECUCION_PROCESO_COUNT_ACTIVIDAD_ECONOMICA");
        return query;
    }

    public static String queryEJECUCION_PROCESO_CONTRATOS() {
        String query = fetchQuery(queryXML).getProperty("queryEJECUCION_PROCESO_CONTRATOS");
        return query;
    }

    public static String queryEJECUCION_PROCESO_COUNT_CONTRATOS() {
        String query = fetchQuery(queryXML).getProperty("queryEJECUCION_PROCESO_COUNT_CONTRATOS");
        return query;
    }

    public static String queryEJECUCION_PROCESO_ESTABLECIMIENTOS() {
        String query = fetchQuery(queryXML).getProperty("queryEJECUCION_PROCESO_ESTABLECIMIENTOS");
        return query;
    }

    public static String queryEJECUCION_PROCESO_COUNT_ESTABLECIMIENTOS() {
        String query = fetchQuery(queryXML).getProperty("queryEJECUCION_PROCESO_COUNT_ESTABLECIMIENTOS");
        return query;
    }

    public static String queryEJECUCION_PROCESO_DIRECCIONES() {
        String query = fetchQuery(queryXML).getProperty("queryEJECUCION_PROCESO_DIRECCIONES");
        return query;
    }

    public static String queryEJECUCION_PROCESO_COUNT_DIRECCIONES() {
        String query = fetchQuery(queryXML).getProperty("queryEJECUCION_PROCESO_COUNT_DIRECCIONES");
        return query;
    }

    public static String queryEJECUCION_PROCESO_TIPO_CONTRATO() {
        String query = fetchQuery(queryXML).getProperty("queryEJECUCION_PROCESO_TIPO_CONTRATO");
        return query;
    }

    public static String queryEJECUCION_PROCESO_COUNT_TIPO_CONTRATO() {
        String query = fetchQuery(queryXML).getProperty("queryEJECUCION_PROCESO_COUNT_TIPO_CONTRATO");
        return query;
    }

    public static String queryEJECUCION_PROCESO_EECC_SPA() {
        String query = fetchQuery(queryXML).getProperty("queryEJECUCION_PROCESO_EECC_SPA");
        return query;
    }

    public static String queryEJECUCION_PROCESO_COUNT_EECC_SPA() {
        String query = fetchQuery(queryXML).getProperty("queryEJECUCION_PROCESO_COUNT_EECC_SPA");
        return query;
    }

    public static String queryEJECUCION_PROCESO_TIPO_CONTACTO() {
        String query = fetchQuery(queryXML).getProperty("queryEJECUCION_PROCESO_TIPO_CONTACTO");
        return query;
    }

    public static String queryEJECUCION_PROCESO_COUNT_TIPO_CONTACTO() {
        String query = fetchQuery(queryXML).getProperty("queryEJECUCION_PROCESO_COUNT_TIPO_CONTACTO");
        return query;
    }
}
