package definitions;

import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;
import com.vimalselvam.cucumber.listener.Reporter;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import pages.EnvironmentArc_Page;
import pages.RubrosInsert_Page;
import properties.PropertiesInit;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

import static org.junit.Assert.assertEquals;

public class TipoRubros_N8_Def {

    public static PropertiesInit prop;
    public static EnvironmentArc_Page environment;
    public static RubrosInsert_Page page;
    public static String insertXML = "src/test/resources/insert/Insert_TipoRubros.xml";
    private String[] origenCS = new String[2];
    private String[] destinoCS = new String[2];

    @Given("^se debe limpiar el ambiente para la ejecucion de pruebas para TIPO_RUBRO caso N_Ocho$")
    public void se_debe_limpiar_el_ambiente_para_la_ejecucion_de_pruebas_para_TIPO_RUBRO_caso_N_Ocho() throws Throwable {
        page.sshConnector(prop.SshHost(), prop.SshPort(), prop.SshUser(), prop.SshPass(), "hdfs dfs -rm -r /data/environment/hdp_lake/stg_arc/tipo_rubro/TO_TRANSFER/*");
        page.sshConnector(prop.SshHostSlave(), prop.SshPortSlave(), prop.SshUserSlave(), prop.SshPassSlave(), "hdfs dfs -rm -r /data/environment/hdp_lake/stg_arc/tipo_rubro/TO_IMPORT/*");
        page.jdbcConnector(prop.OracleDriver(), prop.OracleURL(), prop.OracleUser(), prop.OraclePass(), prop.OracleScheme(), environment.dropTIPO_RUBRO());
        page.jdbcConnector(prop.OracleDriver(), prop.OracleURL(), prop.OracleUser(), prop.OraclePass(), prop.OracleScheme(), environment.createTIPO_RUBRO());
    }

    @When("^se debe insertar el dataset para las tablas de Oracle para TIPO_RUBRO caso N_Ocho$")
    public void se_debe_insertar_el_dataset_para_las_tablas_de_Oracle_para_TIPO_RUBRO_caso_N_Ocho() throws Throwable {
        page.jdbcConnector(prop.OracleDriver(), prop.OracleURL(), prop.OracleUser(), prop.OraclePass(), prop.OracleScheme(), page.insert_ORIGEN(insertXML));
    }

    @Then("^se debe ejecutar el proceso de extraccion para TIPO_RUBRO caso N_Ocho$")
    public void se_debe_ejecutar_el_proceso_de_extraccion_para_TIPO_RUBRO_caso_N_Ocho() throws Throwable {
        page.sshConnector(prop.SshHost(), prop.SshPort(), prop.SshUser(), prop.SshPass(),
                "sh -x /data/environment/hdp_app/load_raw/generic_bash/shells_genericas/shl/Shl_extrae_sqoop.sh 1 20190710 ARC N");

        page.sshConnector(prop.SshHost(), prop.SshPort(), prop.SshUser(), prop.SshPass(),
                "sh -x /data/environment/hdp_app/load_raw/generic_bash/shells_genericas/shl/Shl_extrae_sqoop.sh 1 20190711 ARC N");

        page.sshConnector(prop.SshHostSlave(), prop.SshPortSlave(), prop.SshUserSlave(), prop.SshPassSlave(),
                "sh -x /data/environment/hdp_app/load_raw/generic_bash/shells_genericas/shl/Shl_transfiere_archivo.sh 3 20190710 ARC N");

        page.sshConnector(prop.SshHostSlave(), prop.SshPortSlave(), prop.SshUserSlave(), prop.SshPassSlave(),
                "sh -x /data/environment/hdp_app/load_raw/generic_bash/shells_genericas/shl/Shl_transfiere_archivo.sh 3 20190711 ARC N");
    }

    @When("^hago una consulta al checksum de los archivos de origen TIPO_RUBRO caso N_Ocho$")
    public void hago_una_consulta_al_checksum_de_los_archivos_de_origen_TIPO_RUBRO_caso_N_Ocho() throws Throwable {
        JSch jschSSH = new JSch();
        Session sessionSSH = null;
        ChannelExec channelSSH = null;

        try {
            sessionSSH = jschSSH.getSession(prop.SshUser(), prop.SshHost(), prop.SshPort());
            sessionSSH.setPassword(prop.SshPass());

            // evita preguntar por confirmacion al conectarse
            sessionSSH.setConfig("StrictHostKeyChecking", "no");
            sessionSSH.connect();

            // inicia canal SSH
            channelSSH = (ChannelExec) sessionSSH.openChannel("exec");

            // executar comando
            channelSSH.setCommand("hdfs dfs -checksum /data/environment/hdp_lake/stg_arc/tipo_rubro/TO_TRANSFER/");
            channelSSH.connect();

            // muestra log de la ejecucion
            InputStream in = channelSSH.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(in));
            for (int i = 0; i < 2; i++) {
                origenCS[i] = reader.readLine();
                System.out.println(origenCS[i]);
            }

        } catch (Exception e) {
            e.printStackTrace();

        } finally {
            if (sessionSSH != null) {
                sessionSSH.disconnect();
            }
            if (channelSSH != null) {
                channelSSH.isClosed();
            }
        }
    }

    @When("^hago una consulta al checksum de los archivos transferidos TIPO_RUBRO caso N_Ocho$")
    public void hago_una_consulta_al_checksum_de_los_archivos_transferidos_TIPO_RUBRO_caso_N_Ocho() throws Throwable {
        JSch jschSSH = new JSch();
        Session sessionSSH = null;
        ChannelExec channelSSH = null;

        try {
            sessionSSH = jschSSH.getSession(prop.SshUserSlave(), prop.SshHostSlave(), prop.SshPortSlave());
            sessionSSH.setPassword(prop.SshPassSlave());

            // evita preguntar por confirmacion al conectarse
            sessionSSH.setConfig("StrictHostKeyChecking", "no");
            sessionSSH.connect();

            // inicia canal SSH
            channelSSH = (ChannelExec) sessionSSH.openChannel("exec");

            // executar comando
            channelSSH.setCommand("hdfs dfs -checksum /data/environment/hdp_lake/stg_arc/tipo_rubro/TO_IMPORT/");
            channelSSH.connect();

            // muestra log de la ejecucion
            InputStream in = channelSSH.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(in));
            for (int i = 0; i < 2; i++) {
                destinoCS[i] = reader.readLine();
                System.out.println(destinoCS[i]);
            }

        } catch (Exception e) {
            e.printStackTrace();

        } finally {
            if (sessionSSH != null) {
                sessionSSH.disconnect();
            }
            if (channelSSH != null) {
                channelSSH.isClosed();
            }
        }
    }

    @Then("^valido que los archivos transferidos esten correcto TIPO_RUBRO caso N_Ocho$")
    public void valido_que_los_archivos_transferidos_esten_correcto_TIPO_RUBRO_caso_N_Ocho() throws Throwable {
        for (int i = 0; i < 2; i++) {
            Reporter.addStepLog("Checksum Master= " + origenCS[i]);
            Reporter.addStepLog("Checksum Slave = " + destinoCS[i]);
            Reporter.addStepLog("------------------------------------------------------------------------------");
            assertEquals(origenCS[i], destinoCS[i]);
        }
    }

    @Then("^se debe limpiar el ambiente luego de la ejecucion de pruebas para TIPO_RUBRO caso N_Ocho$")
    public void se_debe_limpiar_el_ambiente_luego_de_la_ejecucion_de_pruebas_para_TIPO_RUBRO_caso_N_Ocho() throws Throwable {
        page.sshConnector(prop.SshHost(), prop.SshPort(), prop.SshUser(), prop.SshPass(), "hdfs dfs -rm -r /data/environment/hdp_lake/stg_arc/tipo_rubro/TO_TRANSFER/*");
        page.sshConnector(prop.SshHostSlave(), prop.SshPortSlave(), prop.SshUserSlave(), prop.SshPassSlave(), "hdfs dfs -rm -r /data/environment/hdp_lake/stg_arc/tipo_rubro/TO_IMPORT/*");
        page.jdbcConnector(prop.OracleDriver(), prop.OracleURL(), prop.OracleUser(), prop.OraclePass(), prop.OracleScheme(), environment.dropTIPO_RUBRO());
    }
}
