package definitions;

import com.vimalselvam.cucumber.listener.Reporter;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import pages.EnvironmentTxs_Page;
import pages.InsertVentasComercio_Page;
import pages.QueryTableTxs_Page;
import properties.PropertiesInit;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

public class VentasComercio_N2_Def {

    public static PropertiesInit prop;
    public static InsertVentasComercio_Page page;
    public static QueryTableTxs_Page query;
    public static EnvironmentTxs_Page environment;
    public Connection connHive;
    public Statement stmtHive;
    public String destinoLOB;

    @When("^se debe limpiar el ambiente para la ejecucion de pruebas de Ventas_Comercios caso N_dos$")
    public void se_debe_limpiar_el_ambiente_para_la_ejecucion_de_pruebas_de_Ventas_Comercios_caso_N_dos() throws Throwable {
        page.jdbcConnector(prop.HiveDriver(), prop.HiveURL(), prop.UserCUR(), prop.PassCUR(), prop.HiveSchemaCurTxProcesadas(), environment.deleteTX_PROCESADAS());
    }

    @Then("^se debe insertar el dataset para las tablas de CUR de Ventas_Comercios caso N_dos$")
    public void se_debe_insertar_el_dataset_para_las_tablas_de_CUR_de_Ventas_Comercios_caso_N_dos() throws Throwable {
        page.jdbcConnector(prop.HiveDriver(), prop.HiveURL(), prop.UserCUR(), prop.PassCUR(), prop.HiveSchemaCurTxProcesadas(), page.insert_TX_PROCESADAS02());
    }

    @When("^hago una consulta a la tabla LOB para vista Ventas_Comercios caso N_dos$")
    public void hago_una_consulta_a_la_tabla_LOB_para_vista_Ventas_Comercios_caso_N_dos() throws Throwable {
        try {
            Class.forName(prop.HiveDriver());
            connHive = DriverManager.getConnection(prop.HiveURL(), prop.UserLOB(), prop.PassLOB());
            connHive.setSchema(prop.HiveSchemaCurTxProcesadas());
            stmtHive = connHive.createStatement();

            ResultSet resCount = stmtHive.executeQuery(query.queryVENTAS_COMERCIO_COUNT());
            while (resCount.next()) {
                destinoLOB = resCount.getString(1);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Then("^valido la visualizacion sea correcta para vista Ventas_Comercios caso N_dos$")
    public void valido_la_visualizacion_sea_correcta_para_vista_Ventas_Comercios_caso_N_dos() throws Throwable {
        Reporter.addStepLog("Consulta Dias = " + destinoLOB);
        Reporter.addStepLog("Esperado Dias = 0");
        System.out.println("Destino " + destinoLOB);
//      assertEquals("0", destinoLOB);
    }

    @Then("^se debe limpiar el ambiente luego de la ejecucion de pruebas para Ventas_Comercios caso N_dos$")
    public void se_debe_limpiar_el_ambiente_luego_de_la_ejecucion_de_pruebas_para_Ventas_Comercios_caso_N_dos() throws Throwable {
        page.jdbcConnector(prop.HiveDriver(), prop.HiveURL(), prop.UserCUR(), prop.PassCUR(), prop.HiveSchemaCurTxProcesadas(), environment.deleteTX_PROCESADAS());
    }
}
