package definitions;

import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;
import com.vimalselvam.cucumber.listener.Reporter;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import pages.Base_Page;
import properties.PropertiesInit;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

public class Contratos_N9_Def {

    public static PropertiesInit prop;
    public static Base_Page page;
    public String logs;

    @When("^Se ejecuta el proceso de extraccion con una fecha erronea para Contratos caso N_Nueve$")
    public void se_ejecuta_el_proceso_de_extraccion_con_una_fecha_erronea_para_Contratos_caso_N_Nueve() throws Throwable {
        page.sshConnector(prop.SshHost(), prop.SshPort(), prop.SshUser(), prop.SshPass(),
                "sh -x /data/environment/hdp_app/load_raw/generic_bash/shells_genericas/shl/Shl_extrae_sqoop.sh 32 00190709 ARC N");
    }

    @Then("^se valida que el proceso de extraccion se haya terminado con error Contratos caso N_Nueve$")
    public void se_valida_que_el_proceso_de_extraccion_se_haya_terminado_con_error_Contratos_caso_N_Nueve() throws Throwable {
        //TODO validar
    }

    @When("^Se ejecuta el proceso de carga STG con una fecha erronea para Contratos caso N_Nueve$")
    public void se_ejecuta_el_proceso_de_carga_STG_con_una_fecha_erronea_para_Contratos_caso_N_Nueve() throws Throwable {
        page.sshConnector(prop.SshHost(), prop.SshPort(), prop.SshUser(), prop.SshPass(),
                "sh -x /data/environment/hdp_app/load_raw/generic_bash/shells_genericas/shl/Shl_crea_tabla_stg.sh 33 S01907j9 ARC N");
    }

    @Then("^se valida que el proceso de carga STG se haya terminado con error Contratos caso N_Nueve$")
    public void se_valida_que_el_proceso_de_carga_STG_se_haya_terminado_con_error_Contratos_caso_N_Nueve() throws Throwable {
        //TODO validar
    }

    @When("^Se ejecuta el proceso de carga RAW con una fecha erronea para Contratos caso N_Nueve$")
    public void se_ejecuta_el_proceso_de_carga_RAW_con_una_fecha_erronea_para_Contratos_caso_N_Nueve() throws Throwable {
        page.sshConnector(prop.SshHost(), prop.SshPort(), prop.SshUser(), prop.SshPass(),
                "sh -x /data/environment/hdp_app/load_raw/generic_bash/shells_genericas/shl/Shl_carga_raw.sh 35 2019:07:09 ARC N");
    }

    @Then("^se valida que el proceso de carga RAW se haya terminado con error Contratos caso N_Nueve$")
    public void se_valida_que_el_proceso_de_carga_RAW_se_haya_terminado_con_error_Contratos_caso_N_Nueve() throws Throwable {
        //TODO validar
    }

    @When("^hago una consulta a los logs generados en RAW para Contratos caso N_Nueve$")
    public void hago_una_consulta_a_los_logs_generados_en_RAW_para_Contratos_caso_N_Nueve() throws Throwable {
        JSch jschSSH = new JSch();
        Session sessionSSH = null;
        ChannelExec channelSSH = null;

        try {
            sessionSSH = jschSSH.getSession(prop.SshUser(), prop.SshHost(), prop.SshPort());
            sessionSSH.setPassword(prop.SshPass());

            // evita preguntar por confirmacion al conectarse
            sessionSSH.setConfig("StrictHostKeyChecking", "no");
            sessionSSH.connect();

            // inicia canal SSH
            channelSSH = (ChannelExec) sessionSSH.openChannel("exec");

            // executar comando
            channelSSH.setCommand("find /data/environment/hdp_log/load_raw/arc/contratos/ -maxdepth 1 -type f | wc -l");
            channelSSH.connect();

            // muestra log de la ejecucion
            InputStream in = channelSSH.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(in));
            int index = 0;
            while ((logs = reader.readLine()) != null) {
                System.out.println(++index + " : " + logs);
                Reporter.addStepLog(logs);
            }

        } catch (Exception e) {
            e.printStackTrace();

        } finally {
            if (sessionSSH != null) {
                sessionSSH.disconnect();
            }
            if (channelSSH != null) {
                channelSSH.isClosed();
            }
        }
    }

    @Then("^valido que la cantidad de logs generados sea correcto para Contratos caso N_Nueve$")
    public void valido_que_la_cantidad_de_logs_generados_sea_correcto_para_Contratos_caso_N_Nueve() throws Throwable {
//        assertEquals("12", logs);
    }
}
