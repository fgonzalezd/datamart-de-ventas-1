package definitions;

import com.jcraft.jsch.ChannelExec;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;
import com.vimalselvam.cucumber.listener.Reporter;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import pages.EnvironmentArc_Page;
import pages.RubrosInsert_Page;
import properties.PropertiesInit;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;

import static org.junit.Assert.assertEquals;

public class ActividadAmex_N7_Def {

    public static PropertiesInit prop;
    public static EnvironmentArc_Page environment;
    public static RubrosInsert_Page page;
    public static String insertXML = "src/test/resources/insert/Insert_ActividadAmex.xml";
    private String origenCS;
    private String destinoCS;

    @Given("^se debe limpiar el ambiente para la ejecucion de pruebas para Actividad_Amex caso N_Siete$")
    public void se_debe_limpiar_el_ambiente_para_la_ejecucion_de_pruebas_para_Actividad_Amex_caso_N_Siete() throws Throwable {
        page.sshConnector(prop.SshHost(), prop.SshPort(), prop.SshUser(), prop.SshPass(), "hdfs dfs -rm -r /data/environment/hdp_lake/stg_arc/actividad_amex/TO_TRANSFER/*");
        page.sshConnector(prop.SshHostSlave(), prop.SshPortSlave(), prop.SshUserSlave(), prop.SshPassSlave(), "hdfs dfs -rm -r /data/environment/hdp_lake/stg_arc/actividad_amex/TO_IMPORT/*");
        page.jdbcConnector(prop.OracleDriver(), prop.OracleURL(), prop.OracleUser(), prop.OraclePass(), prop.OracleScheme(), environment.dropACTIVIDAD_AMEX());
        page.jdbcConnector(prop.OracleDriver(), prop.OracleURL(), prop.OracleUser(), prop.OraclePass(), prop.OracleScheme(), environment.createACTIVIDAD_AMEX());
    }

    @When("^se debe insertar el dataset para las tablas de Oracle para Actividad_Amex caso N_Siete$")
    public void se_debe_insertar_el_dataset_para_las_tablas_de_Oracle_para_Actividad_Amex_caso_N_Siete() throws Throwable {
        page.jdbcConnector(prop.OracleDriver(), prop.OracleURL(), prop.OracleUser(), prop.OraclePass(), prop.OracleScheme(), page.insert_ORIGEN(insertXML));
    }

    @Then("^se debe ejecutar el proceso de extraccion con una fecha del (\\d+)/(\\d+)/(\\d+) para Actividad_Amex caso N_Siete$")
    public void se_debe_ejecutar_el_proceso_de_extraccion_con_una_fecha_del_para_Actividad_Amex_caso_N_Siete(int arg1, int arg2, int arg3) throws Throwable {
        page.sshConnector(prop.SshHost(), prop.SshPort(), prop.SshUser(), prop.SshPass(),
                "sh -x /data/environment/hdp_app/load_raw/generic_bash/shells_genericas/shl/Shl_extrae_sqoop.sh 5 20190710 ARC N");

        page.sshConnector(prop.SshHostSlave(), prop.SshPortSlave(), prop.SshUserSlave(), prop.SshPassSlave(),
                "sh -x /data/environment/hdp_app/load_raw/generic_bash/shells_genericas/shl/Shl_transfiere_archivo.sh 7 20190710 ARC N");
    }

    @When("^hago una consulta al checksum del archivo origen Actividad_Amex caso N_Siete$")
    public void hago_una_consulta_al_checksum_del_archivo_origen_Actividad_Amex_caso_N_Siete() throws Throwable {
        JSch jschSSH = new JSch();
        Session sessionSSH = null;
        ChannelExec channelSSH = null;

        try {
            sessionSSH = jschSSH.getSession(prop.SshUser(), prop.SshHost(), prop.SshPort());
            sessionSSH.setPassword(prop.SshPass());

            // evita preguntar por confirmacion al conectarse
            sessionSSH.setConfig("StrictHostKeyChecking", "no");
            sessionSSH.connect();

            // inicia canal SSH
            channelSSH = (ChannelExec) sessionSSH.openChannel("exec");

            // executar comando
            channelSSH.setCommand("hdfs dfs -checksum /data/environment/hdp_lake/stg_arc/actividad_amex/TO_TRANSFER/actividad_amex_20190710-m-00000");
            channelSSH.connect();

            // muestra log de la ejecucion
            InputStream in = channelSSH.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(in));
            int index = 0;
            while ((origenCS = reader.readLine()) != null) {
                System.out.println(++index + " : " + origenCS);
                Reporter.addStepLog(origenCS);
            }

        } catch (Exception e) {
            e.printStackTrace();

        } finally {
            if (sessionSSH != null) {
                sessionSSH.disconnect();
            }
            if (channelSSH != null) {
                channelSSH.isClosed();
            }
        }
    }

    @When("^hago una consulta al checksum del archivo transferido Actividad_Amex caso N_Siete$")
    public void hago_una_consulta_al_checksum_del_archivo_transferido_Actividad_Amex_caso_N_Siete() throws Throwable {
        JSch jschSSH = new JSch();
        Session sessionSSH = null;
        ChannelExec channelSSH = null;

        try {
            sessionSSH = jschSSH.getSession(prop.SshUserSlave(), prop.SshHostSlave(), prop.SshPortSlave());
            sessionSSH.setPassword(prop.SshPassSlave());

            // evita preguntar por confirmacion al conectarse
            sessionSSH.setConfig("StrictHostKeyChecking", "no");
            sessionSSH.connect();

            // inicia canal SSH
            channelSSH = (ChannelExec) sessionSSH.openChannel("exec");

            // executar comando
            channelSSH.setCommand("hdfs dfs -checksum /data/environment/hdp_lake/stg_arc/actividad_amex/TO_IMPORT/actividad_amex_20190710-m-00000");
            channelSSH.connect();

            // muestra log de la ejecucion
            InputStream in = channelSSH.getInputStream();
            BufferedReader reader = new BufferedReader(new InputStreamReader(in));
            int index = 0;
            while ((destinoCS = reader.readLine()) != null) {
                System.out.println(++index + " : " + destinoCS);
                Reporter.addStepLog(destinoCS);
            }

        } catch (Exception e) {
            e.printStackTrace();

        } finally {
            if (sessionSSH != null) {
                sessionSSH.disconnect();
            }
            if (channelSSH != null) {
                channelSSH.isClosed();
            }
        }
    }

    @Then("^valido que el archivo transferido este correcto Actividad_Amex caso N_Siete$")
    public void valido_que_el_archivo_transferido_este_correcto_Actividad_Amex_caso_N_Siete() throws Throwable {
        Reporter.addStepLog("Checksum Master= " + origenCS);
        Reporter.addStepLog("Checksum Slave = " + destinoCS);
        assertEquals(origenCS, destinoCS);
    }

    @Then("^se debe limpiar el ambiente luego de la ejecucion de pruebas para Actividad_Amex caso N_Siete$")
    public void se_debe_limpiar_el_ambiente_luego_de_la_ejecucion_de_pruebas_para_Actividad_Amex_caso_N_Siete() throws Throwable {
        page.sshConnector(prop.SshHost(), prop.SshPort(), prop.SshUser(), prop.SshPass(), "hdfs dfs -rm -r /data/environment/hdp_lake/stg_arc/actividad_amex/TO_TRANSFER/*");
        page.sshConnector(prop.SshHostSlave(), prop.SshPortSlave(), prop.SshUserSlave(), prop.SshPassSlave(), "hdfs dfs -rm -r /data/environment/hdp_lake/stg_arc/actividad_amex/TO_IMPORT/*");
        page.jdbcConnector(prop.OracleDriver(), prop.OracleURL(), prop.OracleUser(), prop.OraclePass(), prop.OracleScheme(), environment.dropACTIVIDAD_AMEX());
    }
}
