@ctssTipoServicios_N2

Feature: Ctss_Tipo_Servicios N2 - La fecha de los datos ya fue ejecutada OK

  Scenario: Ambientacion para Ctss_Tipo_Servicios "La fecha de los datos ya fue ejecutada OK"
    Given se debe limpiar el ambiente para la ejecucion de pruebas para Ctss_Tipo_Servicios caso N_dos
    When se debe insertar el dataset para las tablas de Oracle para Ctss_Tipo_Servicios caso N_dos
    And se debe insertar el dataset para las tablas de hive para Ctss_Tipo_Servicios caso N_dos
    Then se debe ejecutar el proceso de extraccion para Ctss_Tipo_Servicios caso N_dos

  Scenario: Validacion para tabla Ctss_Tipo_Servicios "La fecha de los datos ya fue ejecutada OK"
    When hago una consulta a la tabla del RAW para tabla Ctss_Tipo_Servicios caso N_dos
    And hago una consulta a la tabla del MCP para tabla Ctss_Tipo_Servicios caso N_dos
    Then valido que la insercion a la tabla RAW sea correcta para tabla Ctss_Tipo_Servicios caso N_dos
    And valido que el registro de insercion en el MCP fue correcto para tabla Ctss_Tipo_Servicios caso N_dos

  Scenario: Validacion de archivos en hdfs para Ctss_Tipo_Servicios "La fecha de los datos ya fue ejecutada OK"
    When hago una consulta a los archivos en TO_IMPORT para Ctss_Tipo_Servicios caso N_dos
    And hago una consulta a los archivos en TO_TRANSFER para Ctss_Tipo_Servicios caso N_dos
    And hago una consulta a los archivos en LOADING para Ctss_Tipo_Servicios caso N_dos
    Then valido que la cantidad de archivos en el hdfs sea correcta para Ctss_Tipo_Servicios caso N_dos

  Scenario: Validacion de logs Ctss_Tipo_Servicios "La fecha de los datos ya fue ejecutada OK"
    When hago una consulta a los logs generados en RAW para Ctss_Tipo_Servicios caso N_dos
    Then valido que la cantidad de logs generados sea correcto para Ctss_Tipo_Servicios caso N_dos

  Scenario: Limpieza ambientacion para Ctss_Tipo_Servicios "La fecha de los datos ya fue ejecutada OK"
    Then se debe limpiar el ambiente luego de la ejecucion de pruebas para Ctss_Tipo_Servicios caso N_dos