@parcat_Credito_N4

Feature: parcat_Credito N4 - Metrica deshabilitada

  Scenario: Ambientacion para "Metrica deshabilitada"
    Given se debe limpiar el ambiente para la ejecucion de pruebas para parcat_Credito caso N_cuatro
    When se debe insertar el dataset para las tablas de Oracle para parcat_Credito caso N_cuatro
    And se debe insertar el dataset para las tablas de hive para parcat_Credito caso N_cuatro
    Then se debe ejecutar el proceso de extraccion con una fecha del 10/7/2019 para parcat_Credito caso N_cuatro

  Scenario: Validacion para tabla parcat_Credito "Metrica deshabilitada"
    When hago una consulta a la tabla del RAW para tabla parcat_Credito caso N_cuatro
    And hago una consulta a la tabla del MCP para tabla parcat_Credito caso N_cuatro
    Then valido que la insercion a la tabla RAW sea correcta para tabla parcat_Credito caso N_cuatro
    And valido que el registro de insercion en el MCP fue correcto para tabla parcat_Credito caso N_cuatro

  Scenario: Limpieza ambientacion para "Metrica deshabilitada"
    Then se debe limpiar el ambiente luego de la ejecucion de pruebas para parcat_Credito caso N_cuatro