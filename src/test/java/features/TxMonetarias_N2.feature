@txMonetarias_N2

Feature: tx_Monetarias N2 - La fecha de los datos ya fue ejecutada OK

  Scenario: Ambientacion para "La fecha de los datos ya fue ejecutada OK"
    Given se debe limpiar el ambiente para la ejecucion de pruebas para tx_Monetarias caso N_dos
    When se debe insertar el dataset para las tablas de Oracle para tx_Monetarias caso N_dos
    And se debe insertar el dataset para las tablas de hive para tx_Monetarias caso N_dos
    Then se debe ejecutar el proceso de extraccion con una fecha del 8/7/2019 para tx_Monetarias caso N_dos

  Scenario: Validacion para tabla tx_Monetarias "La fecha de los datos ya fue ejecutada OK"
    When hago una consulta a la tabla del RAW para tabla tx_Monetarias caso N_dos
    And hago una consulta a la tabla del MCP para tabla tx_Monetarias caso N_dos
    Then valido que la insercion a la tabla RAW sea correcta para tabla tx_Monetarias caso N_dos
    And valido que el registro de insercion en el MCP fue correcto para tabla tx_Monetarias caso N_dos

  Scenario: Limpieza ambientacion para "La fecha de los datos ya fue ejecutada OK"
    Then se debe limpiar el ambiente luego de la ejecucion de pruebas para tx_Monetarias caso N_dos