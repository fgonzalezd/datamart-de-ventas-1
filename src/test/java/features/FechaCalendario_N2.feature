@fechaCalendario_N2

Feature: Fecha_Calendario N2 - La fecha de los datos ya fue ejecutada OK

  Scenario: Ambientacion para "La fecha de los datos ya fue ejecutada OK"
    Given se debe limpiar el ambiente para la ejecucion de pruebas para Fecha_Calendario caso N_dos
    When se debe insertar el dataset para las tablas de RAW para Fecha_Calendario caso N_dos
    And se debe insertar el dataset para las tablas de CUR para Fecha_Calendario caso N_dos
    Then se debe ejecutar el proceso de Fecha_Calendario caso N_dos

  Scenario: Validacion para tabla Fecha_Calendario "La fecha de los datos ya fue ejecutada OK"
    When hago una consulta a la tabla del CUR para tabla Fecha_Calendario caso N_dos
    And hago una consulta a la tabla del MCP para tabla Fecha_Calendario caso N_dos
    Then valido que la insercion a la tabla CUR sea correcta para tabla Fecha_Calendario caso N_dos
    And valido que el registro de insercion en el MCP fue correcto para tabla Fecha_Calendario caso N_dos

  Scenario: Validacion de logs Fecha_Calendario "La fecha de los datos ya fue ejecutada OK"
    When hago una consulta a los logs generados en RAW para Fecha_Calendario caso N_dos
    Then valido que la cantidad de logs generados sea correcto para Fecha_Calendario caso N_dos

  Scenario: Limpieza ambientacion para "La fecha de los datos ya fue ejecutada OK"
    Then se debe limpiar el ambiente luego de la ejecucion de pruebas para Fecha_Calendario caso N_dos