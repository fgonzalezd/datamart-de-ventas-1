@parcat_Debito_N8

Feature: parcat_Debito N8 - Transferencia de multiples archivos DRP

  Scenario: Ambientacion para "Transferencia de multiples archivos DRP"
    Given se debe limpiar el ambiente para la ejecucion de pruebas para parcat_Debito caso N_Ocho
    When se debe insertar el dataset para las tablas de Oracle para parcat_Debito caso N_Ocho
    Then se debe ejecutar el proceso de extraccion para parcat_Debito caso N_Ocho

  Scenario: Validacion de DRP parcat_Debito "Transferencia de multiples archivos DRP"
    When hago una consulta al checksum de los archivos de origen parcat_Debito caso N_Ocho
    And hago una consulta al checksum de los archivos transferidos parcat_Debito caso N_Ocho
    Then valido que los archivos transferidos esten correcto parcat_Debito caso N_Ocho

  Scenario: Limpieza ambientacion para "Transferencia de multiples archivos DRP"
    Then se debe limpiar el ambiente luego de la ejecucion de pruebas para parcat_Debito caso N_Ocho