@tipoRubros_N8

Feature: Tipo_Rubros N8 - Transferencia de multiples archivos DRP

  Scenario: Ambientacion para "Transferencia de multiples archivos DRP"
    Given se debe limpiar el ambiente para la ejecucion de pruebas para TIPO_RUBRO caso N_Ocho
    When se debe insertar el dataset para las tablas de Oracle para TIPO_RUBRO caso N_Ocho
    Then se debe ejecutar el proceso de extraccion para TIPO_RUBRO caso N_Ocho

  Scenario: Validacion de DRP Tipo_Rubros "Transferencia de multiples archivos DRP"
    When hago una consulta al checksum de los archivos de origen TIPO_RUBRO caso N_Ocho
    And hago una consulta al checksum de los archivos transferidos TIPO_RUBRO caso N_Ocho
    Then valido que los archivos transferidos esten correcto TIPO_RUBRO caso N_Ocho

  Scenario: Limpieza ambientacion para "Transferencia de multiples archivos DRP"
    Then se debe limpiar el ambiente luego de la ejecucion de pruebas para TIPO_RUBRO caso N_Ocho