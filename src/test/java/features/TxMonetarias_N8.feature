@txMonetarias_N8

Feature: tx_Monetarias N8 - Transferencia de multiples archivos DRP

  Scenario: Ambientacion para "Transferencia de multiples archivos DRP"
    Given se debe limpiar el ambiente para la ejecucion de pruebas para tx_Monetarias caso N_Ocho
    When se debe insertar el dataset para las tablas de Oracle para tx_Monetarias caso N_Ocho
    Then se debe ejecutar el proceso de extraccion para tx_Monetarias caso N_Ocho

  Scenario: Validacion de DRP tx_Monetarias "Transferencia de multiples archivos DRP"
    When hago una consulta al checksum de los archivos de origen tx_Monetarias caso N_Ocho
    And hago una consulta al checksum de los archivos transferidos tx_Monetarias caso N_Ocho
    Then valido que los archivos transferidos esten correcto tx_Monetarias caso N_Ocho

  Scenario: Limpieza ambientacion para "Transferencia de multiples archivos DRP"
    Then se debe limpiar el ambiente luego de la ejecucion de pruebas para tx_Monetarias caso N_Ocho