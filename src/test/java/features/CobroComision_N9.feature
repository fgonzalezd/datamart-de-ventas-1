@cobroComision_N9

Feature: Cobro_Comision_Retencion N9 - Ingreso de fecha erronea en el parametro

  Scenario: Ejecucion del proceso extraccion Cobro_Comision_Retencion "Ingreso de fecha erronea en el parametro"
    When Se ejecuta el proceso de extraccion con una fecha erronea para Cobro_Comision_Retencion caso N_Nueve
    Then se valida que el proceso de extraccion se haya terminado con error Cobro_Comision_Retencion caso N_Nueve

  Scenario: Ejecucion del proceso STG Cobro_Comision_Retencion "Ingreso de fecha erronea en el parametro"
    When Se ejecuta el proceso de carga STG con una fecha erronea para Cobro_Comision_Retencion caso N_Nueve
    Then se valida que el proceso de carga STG se haya terminado con error Cobro_Comision_Retencion caso N_Nueve

  Scenario: Ejecucion del proceso RAW Cobro_Comision_Retencion "Ingreso de fecha erronea en el parametro"
    When Se ejecuta el proceso de carga RAW con una fecha erronea para Cobro_Comision_Retencion caso N_Nueve
    Then se valida que el proceso de carga RAW se haya terminado con error Cobro_Comision_Retencion caso N_Nueve

  Scenario: Validacion de logs Cobro_Comision_Retencion "Ingreso de fecha erronea en el parametro"
    When hago una consulta a los logs generados en RAW para Cobro_Comision_Retencion caso N_Nueve
    Then valido que la cantidad de logs generados sea correcto para Cobro_Comision_Retencion caso N_Nueve