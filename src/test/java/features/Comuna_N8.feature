@comuna_N8

Feature: Comuna N8 - Transferencia de multiples archivos DRP

  Scenario: Ambientacion para Comuna "Transferencia de multiples archivos DRP"
    Given se debe limpiar el ambiente para la ejecucion de pruebas para Comuna caso N_Ocho
    When se debe insertar el dataset para las tablas de Oracle para Comuna caso N_Ocho
    Then se debe ejecutar el proceso de extraccion y transferencia para Comuna caso N_Ocho

  Scenario: Validacion de DRP Comuna "Transferencia de multiples archivos DRP"
    When hago una consulta al checksum del master Comuna caso N_Ocho
    And hago una consulta al checksum del slave Comuna caso N_Ocho
    Then valido que el archivo transferido este correcto Comuna caso N_Ocho

  Scenario: Validacion de archivos en hdfs para Comuna "Transferencia de multiples archivos DRP"
    When hago una consulta a los archivos en el hdfs del master para Comuna caso N_Ocho
    And hago una consulta a los archivos en el hdfs del slave para Comuna caso N_Ocho
    Then valido que la cantidad de archivos en el hdfs del master sea correcta para Comuna caso N_Ocho
    And valido que la cantidad de archivos en el hdfs del slave sea correcta para Comuna caso N_Ocho

  Scenario: Validacion de logs Comuna "Transferencia de multiples archivos DRP"
    When hago una consulta a los logs generados del master para Comuna caso N_Ocho
    And hago una consulta a los logs generados del slave para Comuna caso N_Ocho
    Then valido que la cantidad de logs generados del master sea correcto para Comuna caso N_Ocho
    And valido que la cantidad de logs generados del slave sea correcto para Comuna caso N_Ocho


  Scenario: Limpieza ambientacion para Comuna "Transferencia de multiples archivos DRP"
    Then se debe limpiar el ambiente luego de la ejecucion de pruebas para Comuna caso N_Ocho