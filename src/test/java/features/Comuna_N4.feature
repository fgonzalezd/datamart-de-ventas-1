@comuna_N4

Feature: Comuna N4 - Metrica deshabilitada

  Scenario: Ambientacion para Comuna "Metrica deshabilitada"
    Given se debe limpiar el ambiente para la ejecucion de pruebas para Comuna caso N_cuatro
    When se debe insertar el dataset para las tablas de Oracle para Comuna caso N_cuatro
    And se debe insertar el dataset para las tablas de hive para Comuna caso N_cuatro
    Then se debe ejecutar el proceso de extraccion para Comuna caso N_cuatro

  Scenario: Validacion para tabla Comuna "Metrica deshabilitada"
    When hago una consulta a la tabla del RAW para tabla Comuna caso N_cuatro
    And hago una consulta a la tabla del MCP para tabla Comuna caso N_cuatro
    Then valido que la insercion a la tabla RAW sea correcta para tabla Comuna caso N_cuatro
    And valido que el registro de insercion en el MCP fue correcto para tabla Comuna caso N_cuatro

  Scenario: Validacion de archivos en hdfs para Comuna "Metrica deshabilitada"
    When hago una consulta a los archivos en TO_IMPORT para Comuna caso N_cuatro
    And hago una consulta a los archivos en TO_TRANSFER para Comuna caso N_cuatro
    And hago una consulta a los archivos en LOADING para Comuna caso N_cuatro
    Then valido que la cantidad de archivos en el hdfs sea correcta para Comuna caso N_cuatro

  Scenario: Validacion de logs Comuna "Metrica deshabilitada"
    When hago una consulta a los logs generados en RAW para Comuna caso N_cuatro
    Then valido que la cantidad de logs generados sea correcto para Comuna caso N_cuatro

  Scenario: Limpieza ambientacion para Comuna "Metrica deshabilitada"
    Then se debe limpiar el ambiente luego de la ejecucion de pruebas para Comuna caso N_cuatro