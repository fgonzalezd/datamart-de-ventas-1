@trxProcesadas_N1

Feature: Transacciones_Procesadas N1 - La fecha de los datos no ha sido ejecutada

  Scenario: Ambientacion para "La fecha de los datos no ha sido ejecutada"
    Given se debe limpiar el ambiente para la ejecucion de pruebas para Transacciones_Procesadas caso N_uno
    When se debe insertar el dataset para las tablas de RAW para Transacciones_Procesadas caso N_uno
    And se debe insertar el dataset para las tablas de CUR para Transacciones_Procesadas caso N_uno
    Then se debe ejecutar el proceso de extraccion con una fecha del 9/7/2019 para Transacciones_Procesadas caso N_uno

  Scenario: Validacion para tabla Transacciones_Procesadas "La fecha de los datos no ha sido ejecutada"
    When hago una consulta a la tabla del CUR para tabla Transacciones_Procesadas caso N_uno
    And hago una consulta a la tabla del MCP para tabla Transacciones_Procesadas caso N_uno
    Then valido que la insercion a la tabla CUR sea correcta para tabla Transacciones_Procesadas caso N_uno
    And valido que el registro de insercion en el MCP fue correcto para tabla Transacciones_Procesadas caso N_uno

  Scenario: Validacion de logs Transacciones_Procesadas "La fecha de los datos no ha sido ejecutada"
    When hago una consulta a los logs generados en RAW para Transacciones_Procesadas caso N_uno
    Then valido que la cantidad de logs generados sea correcto para Transacciones_Procesadas caso N_uno

  Scenario: Limpieza ambientacion para "La fecha de los datos no ha sido ejecutada"
    Then se debe limpiar el ambiente luego de la ejecucion de pruebas para Transacciones_Procesadas caso N_uno