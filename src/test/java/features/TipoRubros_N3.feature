@tipoRubros_N3

Feature: Tipo_Rubros N3 - La fecha de los datos ya fue ejecutada con ERROR

  Scenario: Ambientacion para "La fecha de los datos ya fue ejecutada con ERROR"
    Given se debe limpiar el ambiente para la ejecucion de pruebas para TIPO_RUBRO caso N_tres
    When se debe insertar el dataset para las tablas de Oracle para TIPO_RUBRO caso N_tres
    And se debe insertar el dataset para las tablas de hive para TIPO_RUBRO caso N_tres
    Then se debe ejecutar el proceso de extraccion con una fecha del 8/7/2019 para TIPO_RUBRO caso N_tres

  Scenario: Validacion para tabla Tipo_Rubro "La fecha de los datos ya fue ejecutada con ERROR"
    When hago una consulta a la tabla del RAW para tabla TIPO_RUBRO caso N_tres
    And hago una consulta a la tabla del MCP para tabla TIPO_RUBRO caso N_tres
    Then valido que la insercion a la tabla RAW sea correcta para tabla TIPO_RUBRO caso N_tres
    And valido que el registro de insercion en el MCP fue correcto para tabla TIPO_RUBRO caso N_tres

  Scenario: Limpieza ambientacion para "La fecha de los datos ya fue ejecutada con ERROR"
    Then se debe limpiar el ambiente luego de la ejecucion de pruebas para TIPO_RUBRO caso N_tres