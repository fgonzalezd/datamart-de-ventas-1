@binLocal_N7

Feature: Bin_Local N7 - Transferencia de archivo unico DRP

  Scenario: Ambientacion para Bin_Local "Transferencia de archivo unico DRP"
    Given se debe limpiar el ambiente para la ejecucion de pruebas para Bin_Local caso N_Siete
    When se debe insertar el dataset para las tablas de Oracle para Bin_Local caso N_Siete
    Then se debe ejecutar el proceso de extraccion y transferencia para Bin_Local caso N_Siete

  Scenario: Validacion de DRP Bin_Local "Transferencia de archivo unico DRP"
    When hago una consulta al checksum del master Bin_Local caso N_Siete
    And hago una consulta al checksum del slave Bin_Local caso N_Siete
    Then valido que el archivo transferido este correcto Bin_Local caso N_Siete

  Scenario: Validacion de archivos en hdfs para Bin_Local "Transferencia de archivo unico DRP"
    When hago una consulta a los archivos en el hdfs del master para Bin_Local caso N_Siete
    And hago una consulta a los archivos en el hdfs del slave para Bin_Local caso N_Siete
    Then valido que la cantidad de archivos en el hdfs del master sea correcta para Bin_Local caso N_Siete
    And valido que la cantidad de archivos en el hdfs del slave sea correcta para Bin_Local caso N_Siete

  Scenario: Validacion de logs Bin_Local "Transferencia de archivo unico DRP"
    When hago una consulta a los logs generados del master para Bin_Local caso N_Siete
    And hago una consulta a los logs generados del slave para Bin_Local caso N_Siete
    Then valido que la cantidad de logs generados del master sea correcto para Bin_Local caso N_Siete
    And valido que la cantidad de logs generados del slave sea correcto para Bin_Local caso N_Siete

  Scenario: Limpieza ambientacion para Bin_Local "Transferencia de archivo unico DRP"
    Then se debe limpiar el ambiente luego de la ejecucion de pruebas para Bin_Local caso N_Siete