@establecimientos_N2

Feature: Establecimientos N2 - La fecha de los datos ya fue ejecutada OK

  Scenario: Ambientacion para Establecimientos "La fecha de los datos ya fue ejecutada OK"
    Given se debe limpiar el ambiente para la ejecucion de pruebas para Establecimientos caso N_dos
    When se debe insertar el dataset para las tablas de Oracle para Establecimientos caso N_dos
    And se debe insertar el dataset para las tablas de hive para Establecimientos caso N_dos
    Then se debe ejecutar el proceso de extraccion para Establecimientos caso N_dos

  Scenario: Validacion para tabla Establecimientos "La fecha de los datos ya fue ejecutada OK"
    When hago una consulta a la tabla del RAW para tabla Establecimientos caso N_dos
    And hago una consulta a la tabla del MCP para tabla Establecimientos caso N_dos
    Then valido que la insercion a la tabla RAW sea correcta para tabla Establecimientos caso N_dos
    And valido que el registro de insercion en el MCP fue correcto para tabla Establecimientos caso N_dos

  Scenario: Validacion de archivos en hdfs para Establecimientos "La fecha de los datos ya fue ejecutada OK"
    When hago una consulta a los archivos en TO_IMPORT para Establecimientos caso N_dos
    And hago una consulta a los archivos en TO_TRANSFER para Establecimientos caso N_dos
    And hago una consulta a los archivos en LOADING para Establecimientos caso N_dos
    Then valido que la cantidad de archivos en el hdfs sea correcta para Establecimientos caso N_dos

  Scenario: Validacion de logs Establecimientos "La fecha de los datos ya fue ejecutada OK"
    When hago una consulta a los logs generados en RAW para Establecimientos caso N_dos
    Then valido que la cantidad de logs generados sea correcto para Establecimientos caso N_dos

  Scenario: Limpieza ambientacion para Establecimientos "La fecha de los datos ya fue ejecutada OK"
    Then se debe limpiar el ambiente luego de la ejecucion de pruebas para Establecimientos caso N_dos