@diasFestivos_N9

Feature: dias_Festivos N9 - Ingreso de fecha erronea en el parametro

  Scenario: Ejecucion del proceso extraccion dias_Festivos "Ingreso de fecha erronea en el parametro"
    When Se ejecuta el proceso de extraccion con una fecha erronea para dias_Festivos caso N_Nueve
    Then se valida que el proceso de extraccion se haya terminado con error dias_Festivos caso N_Nueve

  Scenario: Ejecucion del proceso STG dias_Festivos "Ingreso de fecha erronea en el parametro"
    When Se ejecuta el proceso de carga STG con una fecha erronea para dias_Festivos caso N_Nueve
    Then se valida que el proceso de carga STG se haya terminado con error dias_Festivos caso N_Nueve

  Scenario: Ejecucion del proceso RAW dias_Festivos "Ingreso de fecha erronea en el parametro"
    When Se ejecuta el proceso de carga RAW con una fecha erronea para dias_Festivos caso N_Nueve
    Then se valida que el proceso de carga RAW se haya terminado con error dias_Festivos caso N_Nueve

  Scenario: Validacion de logs dias_Festivos "Ingreso de fecha erronea en el parametro"
    When hago una consulta a los logs generados en RAW para dias_Festivos caso N_Nueve
    Then valido que la cantidad de logs generados sea correcto para dias_Festivos caso N_Nueve