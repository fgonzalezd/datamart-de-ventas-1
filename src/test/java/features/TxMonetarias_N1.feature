@txMonetarias_N1

Feature: tx_Monetarias N1 - La fecha de los datos no ha sido ejecutada

  Scenario: Ambientacion para "La fecha de los datos no ha sido ejecutada"
    Given se debe limpiar el ambiente para la ejecucion de pruebas para tx_Monetarias caso N_uno
    When se debe insertar el dataset para las tablas de Oracle para tx_Monetarias caso N_uno
    And se debe insertar el dataset para las tablas de hive para tx_Monetarias caso N_uno
    Then se debe ejecutar el proceso de extraccion con una fecha del 9/7/2019 para tx_Monetarias caso N_uno

  Scenario: Validacion para tabla tx_Monetarias "La fecha de los datos no ha sido ejecutada"
    When hago una consulta a la tabla del RAW para tabla tx_Monetarias caso N_uno
    And hago una consulta a la tabla del MCP para tabla tx_Monetarias caso N_uno
    Then valido que la insercion a la tabla RAW sea correcta para tabla tx_Monetarias caso N_uno
    And valido que el registro de insercion en el MCP fue correcto para tabla tx_Monetarias caso N_uno

  Scenario: Limpieza ambientacion para "La fecha de los datos no ha sido ejecutada"
    Then se debe limpiar el ambiente luego de la ejecucion de pruebas para tx_Monetarias caso N_uno