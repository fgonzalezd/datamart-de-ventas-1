@establecimientos_N8

Feature: Establecimientos N8 - Transferencia de multiples archivos DRP

  Scenario: Ambientacion para Establecimientos "Transferencia de multiples archivos DRP"
    Given se debe limpiar el ambiente para la ejecucion de pruebas para Establecimientos caso N_Ocho
    When se debe insertar el dataset para las tablas de Oracle para Establecimientos caso N_Ocho
    Then se debe ejecutar el proceso de extraccion y transferencia para Establecimientos caso N_Ocho

  Scenario: Validacion de DRP Establecimientos "Transferencia de multiples archivos DRP"
    When hago una consulta al checksum del master Establecimientos caso N_Ocho
    And hago una consulta al checksum del slave Establecimientos caso N_Ocho
    Then valido que el archivo transferido este correcto Establecimientos caso N_Ocho

  Scenario: Validacion de archivos en hdfs para Establecimientos "Transferencia de multiples archivos DRP"
    When hago una consulta a los archivos en el hdfs del master para Establecimientos caso N_Ocho
    And hago una consulta a los archivos en el hdfs del slave para Establecimientos caso N_Ocho
    Then valido que la cantidad de archivos en el hdfs del master sea correcta para Establecimientos caso N_Ocho
    And valido que la cantidad de archivos en el hdfs del slave sea correcta para Establecimientos caso N_Ocho

  Scenario: Validacion de logs Establecimientos "Transferencia de multiples archivos DRP"
    When hago una consulta a los logs generados del master para Establecimientos caso N_Ocho
    And hago una consulta a los logs generados del slave para Establecimientos caso N_Ocho
    Then valido que la cantidad de logs generados del master sea correcto para Establecimientos caso N_Ocho
    And valido que la cantidad de logs generados del slave sea correcto para Establecimientos caso N_Ocho


  Scenario: Limpieza ambientacion para Establecimientos "Transferencia de multiples archivos DRP"
    Then se debe limpiar el ambiente luego de la ejecucion de pruebas para Establecimientos caso N_Ocho