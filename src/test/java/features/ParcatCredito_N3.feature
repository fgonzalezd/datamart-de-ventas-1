@parcat_Credito_N3

Feature: parcat_Credito N3 - La fecha de los datos ya fue ejecutada con ERROR

  Scenario: Ambientacion para "La fecha de los datos ya fue ejecutada con ERROR"
    Given se debe limpiar el ambiente para la ejecucion de pruebas para parcat_Credito caso N_tres
    When se debe insertar el dataset para las tablas de Oracle para parcat_Credito caso N_tres
    And se debe insertar el dataset para las tablas de hive para parcat_Credito caso N_tres
    Then se debe ejecutar el proceso de extraccion con una fecha del 8/7/2019 para parcat_Credito caso N_tres

  Scenario: Validacion para tabla parcat_Credito "La fecha de los datos ya fue ejecutada con ERROR"
    When hago una consulta a la tabla del RAW para tabla parcat_Credito caso N_tres
    And hago una consulta a la tabla del MCP para tabla parcat_Credito caso N_tres
    Then valido que la insercion a la tabla RAW sea correcta para tabla parcat_Credito caso N_tres
    And valido que el registro de insercion en el MCP fue correcto para tabla parcat_Credito caso N_tres

  Scenario: Limpieza ambientacion para "La fecha de los datos ya fue ejecutada con ERROR"
    Then se debe limpiar el ambiente luego de la ejecucion de pruebas para parcat_Credito caso N_tres