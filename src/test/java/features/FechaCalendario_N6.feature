@fechaCalendario_N6

Feature: Fecha_Calendario N6 - Metrica activa con regla OK

  Scenario: Ambientacion para "Metrica activa con regla OK"
    Given se debe limpiar el ambiente para la ejecucion de pruebas para Fecha_Calendario caso N_Seis
    When se debe insertar el dataset para las tablas de RAW para Fecha_Calendario caso N_Seis
    And se debe insertar el dataset para las tablas de CUR para Fecha_Calendario caso N_Seis
    Then se debe ejecutar el proceso de Fecha_Calendario caso N_Seis

  Scenario: Validacion para tabla Fecha_Calendario "Metrica activa con regla OK"
    When hago una consulta a la tabla del CUR para tabla Fecha_Calendario caso N_Seis
    And hago una consulta a la tabla del MCP para tabla Fecha_Calendario caso N_Seis
    Then valido que la insercion a la tabla CUR sea correcta para tabla Fecha_Calendario caso N_Seis
    And valido que el registro de insercion en el MCP fue correcto para tabla Fecha_Calendario caso N_Seis

  Scenario: Validacion de logs Fecha_Calendario "Metrica activa con regla OK"
    When hago una consulta a los logs generados en RAW para Fecha_Calendario caso N_Seis
    Then valido que la cantidad de logs generados sea correcto para Fecha_Calendario caso N_Seis

  Scenario: Limpieza ambientacion para "Metrica activa con regla OK"
    Then se debe limpiar el ambiente luego de la ejecucion de pruebas para Fecha_Calendario caso N_Seis