@ctssTipoServicios_N4

Feature: Ctss_Tipo_Servicios N4 - Metrica deshabilitada

  Scenario: Ambientacion para Ctss_Tipo_Servicios "Metrica deshabilitada"
    Given se debe limpiar el ambiente para la ejecucion de pruebas para Ctss_Tipo_Servicios caso N_cuatro
    When se debe insertar el dataset para las tablas de Oracle para Ctss_Tipo_Servicios caso N_cuatro
    And se debe insertar el dataset para las tablas de hive para Ctss_Tipo_Servicios caso N_cuatro
    Then se debe ejecutar el proceso de extraccion para Ctss_Tipo_Servicios caso N_cuatro

  Scenario: Validacion para tabla Ctss_Tipo_Servicios "Metrica deshabilitada"
    When hago una consulta a la tabla del RAW para tabla Ctss_Tipo_Servicios caso N_cuatro
    And hago una consulta a la tabla del MCP para tabla Ctss_Tipo_Servicios caso N_cuatro
    Then valido que la insercion a la tabla RAW sea correcta para tabla Ctss_Tipo_Servicios caso N_cuatro
    And valido que el registro de insercion en el MCP fue correcto para tabla Ctss_Tipo_Servicios caso N_cuatro

  Scenario: Validacion de archivos en hdfs para Ctss_Tipo_Servicios "Metrica deshabilitada"
    When hago una consulta a los archivos en TO_IMPORT para Ctss_Tipo_Servicios caso N_cuatro
    And hago una consulta a los archivos en TO_TRANSFER para Ctss_Tipo_Servicios caso N_cuatro
    And hago una consulta a los archivos en LOADING para Ctss_Tipo_Servicios caso N_cuatro
    Then valido que la cantidad de archivos en el hdfs sea correcta para Ctss_Tipo_Servicios caso N_cuatro

  Scenario: Validacion de logs Ctss_Tipo_Servicios "Metrica deshabilitada"
    When hago una consulta a los logs generados en RAW para Ctss_Tipo_Servicios caso N_cuatro
    Then valido que la cantidad de logs generados sea correcto para Ctss_Tipo_Servicios caso N_cuatro

  Scenario: Limpieza ambientacion para Ctss_Tipo_Servicios "Metrica deshabilitada"
    Then se debe limpiar el ambiente luego de la ejecucion de pruebas para Ctss_Tipo_Servicios caso N_cuatro